# Readme #
Almacenamiento y Recuperacion de Informacion Taller

# Integrantes #

- Cindy Guerra
- Juan Zapata

# Summary #

1. Parte 1: 
    - Notebook Taller1-Notebook1.ipynb
    - Source: sources/0704.3504.pdf.txt
    
2. Parte 2.
    - Instancia EC2:
        - IP Publica: 35.153.212.253
        - Public DNS: ec2-35-153-212-253.compute-1.amazonaws.com
        - PEM Key: VM/key/jzapatam_ari_taller2.pem
        - Conexion ssh: ssh -i ".ssh/jzapatam_ari_taller2.pem" ec2-user@ec2-35-153-212-253.compute-1.amazonaws.com
        
    - Docker:
        - Docker Compose file: VM/docker/docker-compose.yml
        
    - Solr:
        - URL: http://35.153.212.253:8983
               http://ec2-35-153-212-253.compute-1.amazonaws.com:8983
               
        - Public URL: http://35.153.212.253:8983/solr/taller2/browse